# SyRENDER #

SyRENDER (Scientific Research Experimental Network for Data Enhanced Rendering) is a virtual render farm 
dedicated to produce detailed 3D models & animated visual simulations with data analysis of scientific challenges, 
including COVID-19 imagery and epidemiology.

This would be a re-purposing of a traditional render farm or computer system that produces Computer Generated Imagery (CGI) 
for film and television visual effects.

Potentially, SyRENDER would consist of a high-performance cluster of networked computer systems capable of 
Central Processing Unit (CPU), Graphics Processing Unit (GPU), and Quantum Processing Unit (QPU) rendering.